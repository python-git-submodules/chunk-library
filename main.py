from chunk_library.create_chunk import ChunkCreator

if __name__ == '__main__':
    chunkCreator = ChunkCreator()
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    chunks = chunkCreator.create_chunk(numbers, 4)
    print(chunks)
